document.addEventListener('DOMContentLoaded', onDocumentLoaded);

function onDocumentLoaded() {
    const btn = document.createElement('button');
    // const btnSend = document.getElementById('sendMessage');

    btn.setAttribute('type', 'button');
    btn.setAttribute('id', 'labMouse');

    btn.innerHTML = 'LabMouse';
    btn.onclick = function(){
        alert('Dynamic event')
    };

    document.body.appendChild(btn);
    // btnSend.parentNode.insertBefore(btn, btnSend);

    console.log("TEST")
}

function onClientFormBtnCLick() {
    const btnMouse = document.getElementById('labMouse');

    btnMouse.addEventListener('click', function () {
       console.log('Show me date --> ', Date.now());
    });
    // alert('kuka');
}