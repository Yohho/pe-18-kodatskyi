document.addEventListener('DOMContentLoader', function () {
    const links = document.getElementsByTagName('a');

    for (let element of links) {
        element.addEventListener('click', function (e) {
            if (!confirm('Are u sure?')) {
                e.preventDefault();
            }
        })
    }
});

function onFormSubmit(event) {
    event.preventDefault();

    const form = event.target;
    const data = {};

    for (let element of form) {
        if (element.type === 'text') {
            data [element.name] = element.value;
        }
    }

    console.log('Data --> ', data);
}