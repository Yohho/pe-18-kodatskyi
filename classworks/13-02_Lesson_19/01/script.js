// document.addEventListener("DOMContentLoaded", onReady);
//
// function onReady() {
//     const containers = document.getElementsByClassName("main-menu");
//
//     if (containers.length === 0) {
//         alert("We don`t find anything");
//         return;
//     }
//
//     const parent = containers.item(0);
//     let refElem = null;
//
//     for (let li of parent.children) {
//         if (li.innerHTML === "3.") {
//             refElem = li;
//             break;
//         }
//     }
//
//     if (refElem === null) {
//         console.error("We dont find with this innerHTML...");
//         return;
//     }
//
//     const newLi = document.createElement("li");
//     newLi.innerHTML = "new element!!!";
//
//     parent.insertBefore(newLi, refElem);
// }



document.addEventListener('DOMContentLoaded', onReady);


function onReady() {
    const parent = getContainer();

    if (!parent) {
        console.error('Error');
        return;
    }

    const li = document.createElement('li');

    li.innerHTML = 'dynamicVersion';

    builNewElement('1.', parent, li);

}

function getContainer() {
    const containers = document.getElementsByClassName('main-menu');

    if (!containers.length) {
        console.error('Ничего не найдено!');
        return;
    }

    return containers.item(0);
}

function insertNewElement() {
    const parent = getContainer();

    if (!parent) {
        console.error('Error');
        return;
    }

    const search = document.getElementById('filtration').value;
    const newLi = document.createElement('li');

    newLi.innerHTML = 'new element!!!!';


    console.log( builNewElement(search, parent, newLi) );
}

/**
 *
 **/
function builNewElement(search = '3.', parent, elem) {
    let refElem = null;

    for(let li of parent.children) {
        if (li.innerHTML === search) {
            refElem = li;
            break;
        }
    }

    if (refElem === null) {
        console.error('Не найден элемент с поисковым значением...');
        return;
    }

    return parent.insertBefore(elem, refElem);
}
