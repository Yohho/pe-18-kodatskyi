const customDOM = {
    getAllElement: function () {
        return document.getElementsByTagName("*");
    },
    getById: function(id){
        return document.getElementById(id);
    },
    createElement: function (tagName) {
        return document.createElement(tagName);
    },
    append: function (parent, child) {
        return parent.appendChild(child);
    }
};






(function () {
    const classmates = [
        {
            name: "Ivanova Valentina"
        },
        {
            name: "Chovnik Sergii"
        },
        {
            name: "Aga Tetyana"
        }
    ];

    const ulWrapper = customDOM.getById("classmateWrapper");

    setTimeout(function () {





        classmates.forEach(function (person) {
            const li = customDOM.createElement("li");

            li.innerHTML = person.name;

            customDOM.append(ulWrapper, li);
        });



            // setTimeout(function () {
            //     document.body.style.backgroundColor = "red";
            // }, 500)
    }, 1000);


})();